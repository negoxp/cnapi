<?php
header('Access-Control-Allow-Origin: *');

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
//use Illuminate\Database\Eloquent\Facades\DB;


class V1Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getPost()
    {
    	$data = [];
    	$limit = 10;

    	$posts = DB::table('wp_posts')
          //->selectRaw(DB::raw('sum(sale) as total_sale'))
    			->selectRaw(DB::raw('*'))
          ->where('post_type','=','post')
          ->where('post_status','=','publish')
          ->limit($limit)
          ->orderBy('post_date','desc')
          ->get()
      ;

      $i=0;
      foreach($posts as $p){
				$data [$i]["id"]=  $p->ID; 
				$data [$i]["post_date"]=  $p->post_date; 
				//$data [$i]["post_content"]=  $p->post_content; 
				$data [$i]["post_title"]=  $p->post_title;
				/*
				$data [$i]["post_status"]=  $p->post_status;
				$data [$i]["guid"]=  $p->guid; 

				$postmeta = DB::table('wp_postmeta')->selectRaw(DB::raw('*'))->where('post_id','=',$p->ID)->get();
        foreach($postmeta as $pm){
        	$data [$i][$pm->meta_key]= $pm->meta_value; 
        }

        
        if ($thumbnail = DB::table('wp_posts')->selectRaw(DB::raw('*'))->where('id','=', $data [$i]["_thumbnail_id"])->first()){
        	$data [$i]["guid"] = $thumbnail->guid;
				}
				*/
				$i++;  	
      }


    	//$data []= $posts ;

    	return response()->json($data);
    }
}
